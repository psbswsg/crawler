import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.UnknownHostException;
import java.util.*;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Crawler {

    private static Map<String, Boolean> outputMap = new LinkedHashMap<String, Boolean>();
    private static Map<String, Boolean> findURLMap = new LinkedHashMap<String, Boolean>();
    private static Map<String, Integer> codeMap = new LinkedHashMap<String, Integer>();
    private static Map<String, String>  onSiteRedirectMap = new LinkedHashMap<String, String>();
    private static Map<String, ArrayList<String>>  offSiteRedirectMap = new LinkedHashMap<String, ArrayList<String>>();
    private static Map<String, Integer> sizeMap = new LinkedHashMap<String, Integer>();
    private static Map<String, String> invalidMap = new LinkedHashMap<String, String>();
    private static Map<String, String> htmlMap = new LinkedHashMap<String, String>();
    private static Map<String, Integer> imgMap = new LinkedHashMap<String, Integer>();
    private static Map<String, Date> modifiedMap = new LinkedHashMap<String, Date>();
    private static final String IMG_REX = "<img.*src=(.*?)[^>]*?>";
    private static final String URL_REX = "<a.*?href=[\"']?((https?://)?/?[^\"']+)[\"']?.*?>(.+)</a>";


    public static void main(String[] args) {

        Crawler crawler = new Crawler();

        Scanner in = new Scanner(System.in);
        System.out.println("Please type a valid url that you want to path: \n");
        String targetURL = in.next();
        //String targetURL = "http://comp3310.ddns.net:7880";

        crawler.getURL(targetURL);
        printResult(targetURL);
    }

    private void getURL(String targetURL){
        System.out.println("");
        System.out.println("");
        System.out.println("-----------------------------working on this url---------------------------");


        outputMap.put(targetURL, false);

        String previousURL = "";
        Pattern p = Pattern.compile("(https?://)?[^/\\s]*");
        Matcher m = p.matcher(targetURL);
        if (m.find()) {
            previousURL = m.group();
        }

        crawlLinks(previousURL, outputMap);
    }


    /**
     * Craw all existing url for target URL
     *
     * Algorithm: find a URL, recursive deeply connect until there isn't
     * any new URL existing anymore then connect the second one.
     *
     * Data structure: LinkedHashMap
     *
     * @param targetURL  for example: http://www.google.com.au
     * @param outputMap  a LinkedHashMap save URL(key) and whether this URL
     *                has been craw
     * @return return a map of crawed URL
     * */
    private Map<String, Boolean> crawlLinks(String targetURL, Map<String, Boolean> outputMap) {
        Map<String, Boolean> tempMap = new LinkedHashMap<String, Boolean>();

        for (String key: outputMap.keySet()) {//each time find there is a false url, then try connect that url
            System.out.println("link:" + key + "--------check:"
                    + outputMap.get(key));
            if (!outputMap.get(key))
                connector(targetURL,key, outputMap, tempMap);
                outputMap.replace(key, false, true);
        }
        if (tempMap.isEmpty())
            return outputMap;
        outputMap.putAll(tempMap);
        outputMap.putAll(crawlLinks(targetURL, outputMap));
        return outputMap;
    }
    /**
     * Craw all existing url for target URL
     *
     * Algorithm: find a URL, recursive deeply connect until there isn't
     * any new URL existing anymore then connect the second one.
     *
     * Data structure: LinkedHashMap
     * @param initURL like: "http://comp3310.ddns.net:7880"
     * @param targetURL  for example: http://comp3310.ddns.net:7880/A/10.html
     * @param outputMap  a LinkedHashMap save URL(key) and whether this URL
     *                has been craw
     * */

    private void connector(String initURL, String targetURL, Map<String, Boolean> outputMap, Map<String, Boolean> tempMap){
        try{
            URL target = new URL(targetURL);
            HttpURLConnection httpURLConnection = (HttpURLConnection) target.openConnection();
            httpURLConnection.setRequestMethod("GET");
            httpURLConnection.setInstanceFollowRedirects(false);

            int respondCode = httpURLConnection.getResponseCode();
            codeMap.put(targetURL, respondCode);
            if (respondCode == HttpURLConnection.HTTP_OK){ // successfully connect and get respond
                Date modifiedDate = new Date(httpURLConnection.getLastModified());
                modifiedMap.put(targetURL, modifiedDate);
                InputStream in = httpURLConnection.getInputStream();
                BufferedReader br = new BufferedReader(new InputStreamReader(in));
                Pattern p1 = Pattern.compile(URL_REX);
                Pattern p2 = Pattern.compile(IMG_REX);
                String line = null;
                int count = 0;
                while ((line = br.readLine()) != null){
                    if (line.contains("html"))
                        htmlMap.put(targetURL, "html");
                        sizeMap.put(targetURL, httpURLConnection.getContentLength());
                    Matcher m1 = p1.matcher(line); //find url
                    if (m1.find()){
                        String getString = m1.group(1).trim();
                        findURLMap.put(getString, true);
                        if (!getString.startsWith("http")){ //case: get a path like "/A/10.html"
                            if (getString.startsWith("/"))
                                getString = initURL + getString; // add init url with path => "http://comp3310.ddns.net:7880/A/10.html"
                            else
                                getString = initURL + "/"+ getString; // counter http://comp3310.ddns.net:7880A/10.html error

                        }
                        if(getString.endsWith("/"))
                            getString = getString.substring(0, getString.length() - 1);
                        if (!outputMap.containsKey(getString) && (!tempMap.containsKey(getString))){
                            if (getString.startsWith(initURL))
                                tempMap.put(getString, false);
                            else{
                                if (offSiteRedirectMap.get(targetURL) == null || !offSiteRedirectMap.containsKey(targetURL)){
                                    offSiteRedirectMap.put(targetURL, new ArrayList<String>());
                                }
                                offSiteRedirectMap.get(targetURL).add(getString);
                            }
                        }
                    }
                    Matcher m2 = p2.matcher(line); // find img
                    if (m2.find()){
                        count += 1;
                    }
                }
                imgMap.put(targetURL, count);
            }
            if (respondCode == HttpURLConnection.HTTP_NOT_FOUND){
                invalidMap.put(targetURL, "Invalid");
            }
            if (respondCode<400 && respondCode>=300){
                String location = httpURLConnection.getHeaderField("Location");
                if (location.contains(initURL))
                    onSiteRedirectMap.put(targetURL, location);
            }
            Thread.sleep(2000); // counter 503 respond code
        }catch (MalformedURLException e){
            System.out.println("\n" +
                    "------------important!!!----------" + "\n" +
                    "please type in a valid url" + "\n" +
                    "please type in a valid url" + "\n" +
                    "please type in a valid url" + "\n" +
                    "\n" +
                    "you typed a invalid url and I cannot get useful result below");
        }
        catch (IOException | InterruptedException ignored){

        }
    }

    private static void printResult(String targetURL){

        System.out.println("-------------------------------------- I am a line --------------------------------------");
        System.out.println("\n"+"The total number of distinct URLs found on the site：" + findURLMap.size() +"\n");

        int count = 0;
        for (Map.Entry<String, Integer> mapping : imgMap.entrySet()) {
            count += mapping.getValue();
        }
        System.out.println("The number of html pages and the number of non-html objects: " + htmlMap.size() + "\n" +
                "the number of non-html objects on the site: " + count + "\n");

        int smallest = 0;
        int biggest = 0;
        String big = "";
        String small = "";
        if (sizeMap.size() > 0) {
            Collection<Integer> values = sizeMap.values();
            biggest = Collections.max(values);
            smallest = Collections.min(values);
        }
        for (Map.Entry<String, Integer> mapping : sizeMap.entrySet()) {
            small = (mapping.getValue() == smallest) ? mapping.getKey() : small;
            big = (mapping.getValue() == biggest) ? mapping.getKey() : big;
        }
        System.out.println("The smallest html pages: " + small + " and length is: " + smallest );
        System.out.println("The biggest html pages: " + big + " and length is: " + biggest +"\n");

        Date oldest = modifiedMap.get(targetURL);
        Date newest = modifiedMap.get(targetURL);
        String oldStr = "";
        String newStr = "";
        if (modifiedMap.size()>0){
            Collection<Date> values = modifiedMap.values();
            newest = Collections.max(values);
            oldest = Collections.min(values);
        }
        for (Map.Entry<String, Date> mapping : modifiedMap.entrySet()) {
            oldStr = (mapping.getValue() == oldest) ? mapping.getKey() : oldStr;
            newStr = (mapping.getValue() == newest) ? mapping.getKey() : newStr;
        }
        System.out.println("The oldest page is: " + oldStr + " and modified at " + oldest);
        System.out.println("The newest page is: " + newStr + " and modified at " + newest + "\n");

        System.out.println("Here is a list of " + invalidMap.size() + " invalid URLs (404 not found): ");
        for (Map.Entry<String, String> mapping : invalidMap.entrySet()) {
            System.out.println(mapping.getKey());
        }
        System.out.println("");
        System.out.println("Here is a list of on-site redirected URLs found: ");
        for (Map.Entry<String, String> mapping : onSiteRedirectMap.entrySet()) {
            System.out.println(mapping.getKey() + " ==> " + mapping.getValue());
        }

        System.out.println("\n"+"Here is a list of off-site (including 30x redirects and html references )URLs found: ");
        for (Map.Entry<String, ArrayList<String>> mapping : offSiteRedirectMap.entrySet()) {
            for (String each: mapping.getValue()) {
                System.out.println(mapping.getKey() + " ==> " + each + "\n" +
                        " -------------------------------- which is "+ ((testValid(each)) ? "valid" :"invalid") + " --------------------------------") ;

            }
        }
    }

    private static Boolean testValid(String urlStr) {
        try {
            URL url = new URL(urlStr);
            HttpURLConnection httpURLConnection = (HttpURLConnection) url.openConnection();
            int code = httpURLConnection.getResponseCode();
            if (code != 404)
                return true;
        } catch (UnknownHostException e) {
            return false;
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

}
